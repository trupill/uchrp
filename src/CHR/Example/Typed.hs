{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PostfixOperators #-}
module CHR.Example.Typed where

import Data.TermWithVars
import GHC.Generics

import CHR.Typed

data LeqConstraint = Leq (U String) (U String)
                   | Eq  String String
                   deriving (Show, Generic)

instance Substitutable RuleVar LeqConstraint
instance Unifiable RuleVar     LeqConstraint

data LeqRule = Reflexivity | Antisymmetry | Idempotence | Transitivity

leqRules :: [CHR Integer LeqRule () LeqConstraint]
leqRules = [reflexivity, antisymmetry, idempotence, transitivity]

reflexivity, antisymmetry, idempotence, transitivity :: CHR Integer LeqRule () LeqConstraint
reflexivity  = rule Reflexivity  $ \x     -> [Leq (x?) (x?)] <=> []
antisymmetry = rule Antisymmetry $ \x y   -> [Leq (x?) (y?)] <=> [Eq (x#) (y#)]
idempotence  = rule Idempotence  $ \x y   -> [Leq (x?) (y?)] \\ [Leq (x?) (y?)] <=> []
transitivity = rule Transitivity $ \x y z -> [Leq (x?) (y?), Leq (y?) (z?)] ==> [Leq (x!) (z!)] `priority` 2

exampleLeqConstraints :: [LeqConstraint]
exampleLeqConstraints = [ Leq (T "a") (T "b"), Leq (T "b") (T "c"), Leq (T "c") (T "a") ]

data DijkstraConstraint = Edge   (U String) (U Int) (U String)
                        | Source (U String)
                        | Dist   (U String) (U Int)
                        deriving (Show, Generic)

instance Substitutable RuleVar DijkstraConstraint
instance Unifiable RuleVar     DijkstraConstraint

dijkstraRules :: [CHR Int String Bool DijkstraConstraint]
dijkstraRules = [start, remove, prop]

start, remove, prop :: CHR Int String Bool DijkstraConstraint
start  = rule "start"  $ \s -> [Source (s?)] ==> [Dist (s!) (T 0)] `priority` 1
remove = rule "remove" $ \x d1 d2 -> [Dist (x?) (d1?)] \\ [Dist (x?) (d2?)] <=> []
                                     `when` (d1#) < (d2#) `priority` 1
prop   = rule "prop"   $ \x z d e -> [Dist (x?) (d?), Edge (x?) (e?) (z?)] ==> [Dist (z!) (T ((d#) + (e#)))]
                                     `priority` (d#) + 2

exampleDijsktraConstraints :: [DijkstraConstraint]
exampleDijsktraConstraints = [ Edge (T "a") (T 1) (T "b")
                             , Edge (T "b") (T 3) (T "d")
                             , Edge (T "a") (T 2) (T "c")
                             , Edge (T "c") (T 1) (T "d")
                             , Source (T "a") ]
