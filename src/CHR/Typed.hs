{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE DeriveGeneric #-}
module CHR.Typed (
-- * Terms with logical variables
  U
, V
, pattern T
, pattern V
, pattern NmV
-- ** Type classes you need to implement
, CHRSubstitutable
, CHRUnifiable
-- * Typed Constraint Handling Rules
, MonadicCHR
, CHR
, ExtraPass(..)
, ExtraPassStep(..)
, extraPassByMatching
, Guard(..)
, Solution(..)
, Justification(..)
, runMonadicCHR
, runCHR
-- ** Type CHR builders
, rule
, (?)
, (!)
, (#)
-- ** Nicer syntax for rules
, (\\)
, (<=>)
, (<=!>)
, (<=>..)
, (<=!>..)
, (==>)
, (==!>)
, (==>..)
, (==!>..)
, priority
, when
-- * For internal use only
, TypedRule(..)
, RuleVar(..)
) where

import Data.Dynamic
import Data.Functor.Identity
import Data.TermWithVars
import GHC.Generics

import CHR.Untyped ( Rule(..), Guard(..)
                   , Solution(..), Justification(..), runCHRLoop
                   , ExtraPass(..), ExtraPassStep(..), extraPassByMatching)

-- | When defining a data type, mark with this types those
-- positions where unification may happen.
type U = TermWithVars RuleVar
-- | Injects a non-variable term inside a `U` type.
pattern T x = Term x
-- | Represents the types of variables used in typed CHRs.
-- Use it when you need to give a type signature for a
-- logical variable appearing in a CHR.
type V = TypedRuleVar
-- | Injects a variable term inside a `V` type.
pattern V x   = Var (RuleVar x)
pattern NmV x = Var (NamedRuleVar x)

-- | Representation of a Constraint Handling Rule with Priorities
--
--   * with effects comming from `Monad m`,
--   * whose priorities are stated via the `Ord` instance of `o`,
--   * whose rules are identified by elements of `i`,
--   * whose guards are describes by values of type `g`,
--   * deals with constraints of type `c`.
type MonadicCHR m o i g c = Rule m RuleVar o i g c

-- | Representation of a Constraint Handling Rule with Priorities
--   without monadic effects.
type CHR = Rule Identity RuleVar

type CHRSubstitutable = Substitutable RuleVar
type CHRUnifiable     = Unifiable RuleVar

-- | A general CHR rule of the form:
--
-- > priority :: given \ wanted <=> guard | body
--
-- where the execution of the rule may have side effects given by `m`.
--
-- In CHR literature, these are called simpagation rules.
data TypedRule m o g c = TypedRule { tgiven  :: [c]
                                   , twanted :: [c]
                                   , tguard  :: g
                                   , tbody   :: m [[c]]
                                   , tprio   :: o }

-- | Runs a set of CHRs over the given initial constraints.
-- The result is the final constraint store.
runCHR :: (Ord o, CHRUnifiable c, Guard g)
       => [CHR o i g c] -> [ExtraPass Identity i c]
       -> [c] -> [Solution i c]
runCHR rules es cs = runIdentity $ runCHRLoop rules es cs

-- | Runs a set of CHRs over the given initial constraints.
-- The result is the final constraint store.
runMonadicCHR :: (Monad m, Ord o, CHRUnifiable c, Guard g)
              => [MonadicCHR m o i g c] -> [ExtraPass m i c]
              -> [c] -> m [Solution i c]
runMonadicCHR = runCHRLoop

-- CONVERSION TO NORMAL RULES
-- ==========================

data TypedRuleVar a = TypedRuleVar RuleVar (U a)
                    deriving (Eq, Ord, Show, Generic, Typeable)
data RuleVar        = RuleVar Int | NamedRuleVar String
                    deriving (Eq, Ord, Generic, Typeable)

instance Show RuleVar where
  show (RuleVar i)      = '$' : (show i)
  show (NamedRuleVar n) = '$' : n

-- | Obtains the logical variable represented by an argument
-- in a typed CHR rule. Use this combinator /only/ in the
-- left-hand side of a rule.
(?) :: TypedRuleVar a -> U a
(?) (TypedRuleVar v _) = Var v

-- | Obtains the unified term represented by an argument
-- in a typed CHR rule. Use this combinator /only/ in the
-- right-hand side of a rule.
(!) :: TypedRuleVar a -> U a
(!) (TypedRuleVar _ x) = x

-- | Obtains the /ground/ term represented by an argument
-- in a typed CHR rule. Use this combinator /only/ in the
-- right-hand side of a rule.
(#) :: TypedRuleVar a -> a
(#) (TypedRuleVar _ x) = unTerm x

-- | Head a typed CHR with a call to `rule` in order to be
-- usable by the CHR engine.
rule :: TypedToCHR x  m o g c => i -> x -> MonadicCHR m o i g c
rule n x = Rule { name   = n
                , given  = tgiven  (rule_ 0 x lhsError)
                , wanted = twanted (rule_ 0 x lhsError)
                , guard  = \s -> tguard (rule_ 0 x s)
                , body   = \s -> tbody  (rule_ 0 x s)
                , prio   = \s -> tprio  (rule_ 0 x s) }
  where lhsError :: Substitution RuleVar = error "You should not access variable result here!"

class TypedToCHR x m o g c | x -> m o g c where
  rule_ :: Int -> x -> Substitution RuleVar -> TypedRule m o g c

instance TypedToCHR (TypedRule m o g c) m o g c where
  rule_ _ x _ = x

instance (TypedToCHR x m o g c, Typeable a)
         => TypedToCHR (TypedRuleVar a -> x) m o g c where
  rule_ n f s = let v  = RuleVar n
                    tv = TypedRuleVar v (s @@! v :: TermWithVars RuleVar a)
                 in rule_ (n+1) (f tv) s


-- TYPED CHR RULES BUILDERS
-- ========================

data Simpagation c = Simpagation [c] [c]
infixl 8 \\
-- | Defines a pair of simplified and propagated constraints.
(\\) :: [c] -> [c] -> Simpagation c
(\\) = Simpagation

infixl 6 <=>, <=!>, <=>.., <=!>..
-- | Used to define both simplification and simpagation rules.
class Iff c t | t -> c where
  -- | Defined a simplification or a simpagation rule with monadic effects and back-tracking.
  (<=!>..) :: (Num o, Guard g) => t -> m [[c]] -> TypedRule m o g c
  -- | Defines a simplification or simpagation rule with monadic effects.
  (<=!>) :: (Monad m, Num o, Guard g) => t -> m [c] -> TypedRule m o g c
  ws <=!> b = ws <=!>.. (do { b' <- b ; return [b'] })
  -- | Defines a simplification or simpagation rule with back-tracking.
  (<=>..) :: (Monad m, Num o, Guard g) => t -> [[c]] -> TypedRule m o g c
  ws <=>.. b = ws <=!>.. (return b)
  -- | Defines a simplification or a simpagation rule.
  (<=>)  :: (Monad m, Num o, Guard g) => t -> [c] -> TypedRule m o g c
  ws <=> b = ws <=!>.. (return [b])
instance Iff c [c] where
  ws <=!>.. b = TypedRule { tgiven = [], twanted = ws, tguard = trivial, tbody = b, tprio = 0 }
instance Iff c (Simpagation c) where
  (Simpagation gs ws) <=!>.. b = TypedRule { tgiven = gs, twanted = ws, tguard = trivial, tbody = b, tprio = 0 }

infixl 6 ==>, ==!>, ==>.., ==!>..
-- | Defines a propagation rule with monadic effects and back-tracking.
(==!>..) :: (Num o, Guard g) => [c] -> m [[c]] -> TypedRule m o g c
gs ==!>.. b = TypedRule { tgiven = gs, twanted = [], tguard = trivial, tbody = b, tprio = 0 }
-- | Defines a propagation rule with monadic effects.
(==!>) :: (Monad m, Num o, Guard g) => [c] -> m [c] -> TypedRule m o g c
gs ==!> b = gs ==!>.. (do { b' <- b ; return [b'] })
-- | Defines a propagation rule with back-tracking.
(==>..) :: (Monad m, Num o, Guard g) => [c] -> [[c]] -> TypedRule m o g c
gs ==>.. b = gs ==!>.. (return b)
-- | Defines a propagation rule.
(==>) :: (Monad m, Num o, Guard g) => [c] -> [c] -> TypedRule m o g c
gs ==> b = gs ==!>.. (return [b])

infixl 2 `priority`
-- | States the priority of a rule.
priority :: TypedRule m a g c -> o -> TypedRule m o g c
priority r p = r { tprio = p }

infixl 2 `when`
-- | Adds a guard to a CHR rule.
when :: Guard g => TypedRule m o g c -> g -> TypedRule m o g c
when r g = r { tguard = g }
