{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeOperators #-}
module Data.TermWithVars (
-- * Terms with logical variables
  TermWithVars(..)
-- * Substitution
, Substitution
, emptySubstitution
, (@@?)
, (@@!)
, (@@+)
, Substitutable(..)
, substs
, ssubst
-- * Unification
, Unifiable(..)
, unifyEq
) where

import Control.Applicative
import Data.Bifunctor
import Data.Dynamic
import qualified Data.Map as M
import Data.Maybe (fromJust)
import Data.Proxy
import GHC.Generics

-- | Substitution of logical variables `v` to terms
data Substitution v = Substitution (M.Map v Dynamic)

-- | Looks up the value of a logical variable into
-- a substitution. Returns `Nothing` if the variable
-- cannot be found or the term does not have the
-- right type.
(@@?) :: (Ord v, Typeable a) => Substitution v -> v -> Maybe a
Substitution s @@? v = M.lookup v s >>= fromDynamic

-- | Version of `@@` which fails in case the variable
-- is not found or has the wrong type.
(@@!) :: (Ord v, Typeable a) => Substitution v -> v -> a
s @@! v = fromJust (s @@? v)

-- | Version of `@@` used when the result type cannot
-- be inferred and must be given instead as a `Proxy`.
(@@+) :: (Ord v, Typeable a) => Substitution v -> v -> Proxy a -> Maybe a
(s @@+ v) _ = s @@? v

-- | Empty substitution.
emptySubstitution :: Substitution v
emptySubstitution = Substitution M.empty

-- | Merge substitutions.
mergeSubstitutions :: Ord v => Substitution v -> Substitution v -> Substitution v
mergeSubstitutions (Substitution x) (Substitution y)
  = Substitution $ M.unionWith (error "Duplicated key") x y

-- | A substitution with only one assignment.
singleSubstitution :: Typeable t => v -> t -> Substitution v
singleSubstitution v t = Substitution $ M.singleton v (toDyn t)

-- | Represents either a logical variable or a term.
data TermWithVars v t = Var  { unVar  :: v }
                      | Term { unTerm :: t }
                      deriving (Eq, Ord, Functor, Generic, Typeable)

instance (Show v, Show t) => Show (TermWithVars v t) where
  showsPrec n (Var  v) = showsPrec n v
  showsPrec n (Term t) = showsPrec n t

instance Bifunctor TermWithVars where
  bimap f _ (Var  v) = Var  (f v)
  bimap _ g (Term t) = Term (g t)

-- | Terms which allow substituting a `Dynamic` value
-- inside a logical variable `v`.
class Substitutable v t where
  -- | Perform substitution of a logical variable in a term.
  subst :: v -> Dynamic -> t -> t
  default subst :: (Generic t, GSubstitutable v (Rep t))
                => v -> Dynamic -> t -> t
  subst v e = to . gsubst v e . from

instance (Eq v, Typeable v, Typeable t, Substitutable v t)
         => Substitutable v (TermWithVars v t) where
  subst v e t@(Var w)
    | v == w, Just t' <- fromDynamic e = t'
    | otherwise = t
  subst v e (Term t) = Term (subst v e t)

-- | Perform several substitutions of logical variables
-- over the same term.
substs :: (Foldable f, Substitutable v t) => f (v,Dynamic) -> t -> t
substs ss t = foldr (\(v,e) c -> subst v e c) t ss

-- | Apply a `Substitution` to a term.
ssubst :: Substitutable v t => Substitution v -> t -> t
ssubst (Substitution ss) = substs (M.toList ss)

-- | Terms which can be checked for unification, whose
-- logical variables are of type `v`.
class Substitutable v t => Unifiable v t where
  -- | Find a (possibly failing) unifying substitution.
  unify :: (Alternative m, Monad m) => t -> t -> m (Substitution v)
  default unify :: (Alternative m, Monad m,
                    Generic t, GUnifiable v (Rep t))
                => t -> t -> m (Substitution v)
  unify t s = gunify (from t) (from s)

instance (Eq v, Typeable v, Typeable t, Unifiable v t)
         => Unifiable v (TermWithVars v t) where
  unify (Var v)  (Var w)
    | v == w              = pure emptySubstitution
  unify (Var v)  t        = pure $ singleSubstitution v t
  unify t        (Var v)  = pure $ singleSubstitution v t
  unify (Term t) (Term s) = unify t s

-- GENERIC IMPLEMENTATIONS
-- =======================
class GSubstitutable v f where
  gsubst :: v -> Dynamic -> f c -> f c

instance Substitutable v t => GSubstitutable v (K1 i t) where
  gsubst v e (K1 t) = K1 (subst v e t)
instance GSubstitutable v f => GSubstitutable v (M1 i c f) where
  gsubst v e (M1 t) = M1 (gsubst v e t)
instance GSubstitutable v U1 where
  gsubst _ _ U1 = U1
instance GSubstitutable v V1 where
  gsubst _ _ x = x
instance (GSubstitutable v f, GSubstitutable v g)
         => GSubstitutable v (f :*: g) where
  gsubst v e (x :*: y) = gsubst v e x :*: gsubst v e y
instance (GSubstitutable v f, GSubstitutable v g)
         => GSubstitutable v (f :+: g) where
  gsubst v e (L1 x) = L1 (gsubst v e x)
  gsubst v e (R1 x) = R1 (gsubst v e x)

gsubsts :: GSubstitutable v f => Substitution v -> f c -> f c
gsubsts (Substitution ss) t = foldr (\(v,e) c -> gsubst v e c) t (M.toList ss)

class GSubstitutable v f => GUnifiable v f where
  gunify :: (Alternative m, Monad m) => f c -> f c -> m (Substitution v)

instance Unifiable v t => GUnifiable v (K1 i t) where
  gunify (K1 t) (K1 s) = unify t s
instance GUnifiable v f => GUnifiable v (M1 i c f) where
  gunify (M1 t) (M1 s) = gunify t s
instance GUnifiable v U1 where
  gunify _ _ = pure emptySubstitution
instance GUnifiable v V1 where
  gunify _ _ = pure emptySubstitution
instance (Ord v, GUnifiable v f, GUnifiable v g)
         => GUnifiable v (f :*: g) where
  gunify (x :*: y) (x' :*: y') = do
    ss  <- gunify y y'
    ss' <- gunify (gsubsts ss x) (gsubsts ss x')
    return $ mergeSubstitutions ss' ss
instance (GUnifiable v f, GUnifiable v g)
         => GUnifiable v (f :+: g) where
  gunify (L1 t) (L1 s) = gunify t s
  gunify (R1 t) (R1 s) = gunify t s
  gunify _      _      = empty

-- BASIC INSTANCES
-- ===============
unifyEq :: (Alternative m, Eq a) => a -> a -> m (Substitution v)
unifyEq x y | x == y    = pure emptySubstitution
            | otherwise = empty

instance Substitutable v Char where
  subst _ _ = id
instance Unifiable v Char where
  unify = unifyEq
instance Substitutable v Bool where
  subst _ _ = id
instance Unifiable v Bool where
  unify = unifyEq
instance Substitutable v Integer where
  subst _ _ = id
instance Unifiable v Integer where
  unify = unifyEq
instance Substitutable v Int where
  subst _ _ = id
instance Unifiable v Int where
  unify = unifyEq
instance Substitutable v t => Substitutable v [t] where
  subst v e = map (subst v e)
instance (Ord v, Unifiable v t) => Unifiable v [t] where
  unify []     []     = return emptySubstitution
  unify (x:xs) (y:ys) = do
    ss  <- unify xs ys
    ss' <- unify (ssubst ss x) (ssubst ss y)
    return $ mergeSubstitutions ss' ss
  unify _      _      = empty
instance Substitutable v t => Substitutable v (Maybe t) where
  subst v e = fmap (subst v e)
instance (Ord v, Unifiable v t) => Unifiable v (Maybe t) where
  unify Nothing  Nothing  = return emptySubstitution
  unify (Just x) (Just y) = unify x y
  unify _        _        = empty
instance (Substitutable v a, Substitutable v b)
         => Substitutable v (a, b) where
  subst v e (x,y) = (subst v e x, subst v e y)
instance (Ord v, Unifiable v a, Unifiable v b)
         => Unifiable v (a, b) where
  unify (a1, b1) (a2, b2) = do
    ss  <- unify a1 a2
    ss' <- unify (ssubst ss b1) (ssubst ss b2)
    return $ mergeSubstitutions ss' ss
instance (Substitutable v a, Substitutable v b, Substitutable v c)
         => Substitutable v (a, b, c) where
  subst v e (x,y,z) = (subst v e x, subst v e y, subst v e z)
instance (Ord v, Unifiable v a, Unifiable v b, Unifiable v c)
         => Unifiable v (a, b, c) where
  unify (a1, b1, c1) (a2, b2, c2) = do
    ss  <- unify a1 a2
    ss' <- unify (ssubst ss (b1,c1)) (ssubst ss (b2,c2))
    return $ mergeSubstitutions ss' ss
instance (Substitutable v a, Substitutable v b, Substitutable v c, Substitutable v d)
         => Substitutable v (a, b, c, d) where
  subst v e (x,y,z,w) = (subst v e x, subst v e y, subst v e z, subst v e w)
instance (Ord v, Unifiable v a, Unifiable v b, Unifiable v c, Unifiable v d)
         => Unifiable v (a, b, c, d) where
  unify (a1, b1, c1, d1) (a2, b2, c2, d2) = do
    ss   <- unify a1 a2
    ss'  <- unify (ssubst ss (b1,c1,d1)) (ssubst ss (b2,c2,d2))
    return $ mergeSubstitutions ss' ss
