{-# LANGUAGE FlexibleInstances #-}
module Data.PartialOrd where

class Eq a => PartialOrd a where
  compare_ :: a -> a -> Maybe Ordering

  (<?)  :: a -> a -> Maybe Bool
  x <?  y = (== LT) <$> compare_ x y
  (<=?) :: a -> a -> Maybe Bool
  x <=? y= (\c -> c == LT || c == EQ) <$> compare_ x y
  (>?)  :: a -> a -> Maybe Bool
  x >?  y = (== GT) <$> compare_ x y
  (>=?) :: a -> a -> Maybe Bool
  x >=? y = (\c -> c == GT || c == EQ) <$> compare_ x y

-- Base types
instance PartialOrd () where
  compare_ _ _ = Just EQ
instance PartialOrd Bool where
  compare_ x y = Just $ compare x y
instance PartialOrd Int where
  compare_ x y = Just $ compare x y
instance PartialOrd Integer where
  compare_ x y = Just $ compare x y
instance PartialOrd Char where
  compare_ x y = Just $ compare x y
instance PartialOrd Float where
  compare_ x y = Just $ compare x y
instance PartialOrd Double where
  compare_ x y = Just $ compare x y

-- Basic combinations
instance (PartialOrd a, PartialOrd b) => PartialOrd (a,b) where
  compare_ (x1,y1) (x2,y2)
    = case compare_ x1 x2 of
        Just EQ -> compare_ y1 y2
        other   -> other
instance (PartialOrd a, PartialOrd b, PartialOrd c) => PartialOrd (a,b,c) where
  compare_ (x1,y1,z1) (x2,y2,z2)
    = case compare_ x1 x2 of
        Just EQ -> compare_ (y1,z1) (y2,z2)
        other   -> other
